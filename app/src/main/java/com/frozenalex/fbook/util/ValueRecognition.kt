package com.frozenalex.fbook.util

import android.icu.util.RangeValueIterator
import android.media.Rating

import com.frozenalex.fbook.data.model.Book

import org.jsoup.Jsoup
import org.jsoup.nodes.Element

import java.text.SimpleDateFormat
import java.util.*

import kotlin.collections.ArrayList

/**
* Created by Alex Uskov on 7/13/17.
*/
object ValueRecognition {

    // Date on the website
    private val dateFormat = Regex("""(\d{2}\.\d{2}\.\d{4}\ \d{2}:\d{2})""")

    // Rating on the website
    private val rating =  Regex("""([\d,.]*)/(\d*)""")

    private val dateForm = SimpleDateFormat("dd.MM.yyyy HH:mm")

    private val idParse = Regex("""id(\d*)""")

    private val titleParse = Regex("""— (.*)""")

    fun parseDate(date:String) : Date? {
        val dateString = dateFormat.find(date)?.value
        try {
            return dateForm.parse(dateString)
        } catch (e:Exception){
            return null
        }
    }

    fun parseRating(ratingString: String):Float? {
        var score = rating.find(ratingString)?.groups?.get(1)?.value
        try {
            return score?.toFloat()
        } catch (e: Exception){
            return null
        }

    }

    fun parseLibrary(html: String): ArrayList<Book> {
        var books = ArrayList<Book>()
        var doc = Jsoup.parse(html)

        // TODO: Fix this mess
        // Get html containers
        var book_title = doc.getElementsByClass("news_title")
        var book_top = doc.getElementsByClass("news_body")
        var book_bottom = doc.getElementsByClass("news_footer")

        if (book_bottom.size == book_top.size) {
            for (i in book_top.indices) {
                var author = book_top[i].select("a[href^=/author/]").first().text()
                var title = titleParse.find(book_title[i].child(0).text())?.groups?.get(1)?.value

                var id = parseId(book_title[i].child(0).attr("href"))
                var description = book_top[i].select("[itemprop=\"description\"]").first().text()

                if (title != null) {
                    var book = Book(id, title, author, description)
                    books.add(book)
                }
            }
        }
        return books
    }

    fun parseId(url: String): Long {
        var score = idParse.find(url)?.groups?.get(1)?.value

        if (score != null) {
            return score.toLong()
        } else {
            return 1
        }
    }


}