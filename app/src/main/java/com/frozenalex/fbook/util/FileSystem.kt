package com.frozenalex.fbook.util

import android.Manifest
import android.app.DownloadManager
import android.app.Notification
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.frozenalex.fbook.data.model.Book
import java.io.File
import com.frozenalex.fbook.activities.SettingsActivity
import android.preference.PreferenceManager
import android.content.SharedPreferences
import android.net.Network


object FileSystem {
    fun getBookStorageDir(context: Context, bookName: String): File {
        // Get the directory for the app's private pictures directory.
        val file = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS
        ), bookName)
        if (!file.mkdirs()) {

        }
        return file
    }

    // FIXME: Make my own download manager...
    fun downloadBook(context: Context, book: Book) {

        var prefs = PreferenceManager.getDefaultSharedPreferences(context);

        //var notifications = prefs.getBoolean("downloads_over_wifi", true);
        var authorFolder = prefs.getBoolean("downloads_author_folder", true);

        // Get download manager service
        var manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

        var destinationFolder = "";

        if (authorFolder) {
            destinationFolder = "/Books/FBook/" + book.author + "/";
        } else {
            destinationFolder = "/Books/FBook/"
        }

//        val f = File(destinationFolder + book.author + " - " + book.title+ ".fb2.zip")
//        if (f.exists() && !f.isDirectory()) {
//
//        }

        var request = DownloadManager.Request(Uri.parse("https://fantasy-worlds.org/lib/id" + book.id + "/download/"))
        request.setDescription(book.author + " - " + book.title)
        request.setVisibleInDownloadsUi(true)

        ///request.setAllowedNetworkTypes()
        request.setDestinationInExternalPublicDir(destinationFolder, book.author + " - " + book.title + ".fb2.zip")
        manager.enqueue(request)
    }


    fun downloadCover(context: Context, book: Book) {

        var prefs = PreferenceManager.getDefaultSharedPreferences(context);

        //var notifications = prefs.getBoolean("downloads_over_wifi", true);
        var authorFolder = prefs.getBoolean("downloads_author_folder", true);
//        prefs.getBoolean("downloads_show_notifications", true);

        // Get download manager service
        var manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

        var destinationFolder = "";

        if (authorFolder) {
            destinationFolder = "/Books/FBook/" + book.author + "/";
        } else {
            destinationFolder = "/Books/FBook/"
        }

        var request = DownloadManager.Request(Uri.parse(book.coverURL))
        request.setDescription("Обложка к: " + book.title)
        request.setVisibleInDownloadsUi(true)

        request.setDestinationInExternalPublicDir(destinationFolder, book.author + " - " + book.title + ".jpg")
        manager.enqueue(request)
    }
}