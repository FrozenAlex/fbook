package com.frozenalex.fbook.data.model

/**
 * Created by Alex Uskov on 7/23/17.
 */
class SQLiteHelper {

    companion object {
        val DATABASE_NAME = "booksDatabase"
        val DATABASE_VERSION = 1

        val TABLE_BOOKS = "books"
        val TABLE_AUTHORS = "authors"
        val TABLE_DOWNLOADS = "downloads"

        val KEY_POST_ID = "id"
        val KEY_POST_USER_ID_FK = "userId"
        val KEY_POST_TEXT = "text"

        // User Table Columns
        val KEY_USER_ID = "id"
        val KEY_USER_NAME = "userName"
        val KEY_USER_PROFILE_PICTURE_URL = "profilePictureUrl"

    }
}