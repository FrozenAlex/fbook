package com.frozenalex.fbook.activities

import android.Manifest
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.GridLayout
import com.frozenalex.fbook.data.model.Book
import com.frozenalex.fbook.util.ValueRecognition
import android.os.Handler
import android.widget.Toast
import com.frozenalex.fbook.R
import com.frozenalex.fbook.BookListAdapter
import com.frozenalex.fbook.network.NetworkHelper
import android.content.Context
import android.os.PersistableBundle
import android.app.DownloadManager
import com.frozenalex.fbook.util.FileSystem
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

import android.app.SearchManager;
import android.view.*
import android.widget.SearchView;
import com.frozenalex.fbook.download.DownloadService

import java.net.URLEncoder
import java.util.concurrent.Executors
import java.util.concurrent.Future


class MainActivity : AppCompatActivity() {
    //val EXTRA_MESSAGE = "com.frozenalex.fbook.ViewBook"

    private val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1

    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var mBooks: ArrayList<Book> = ArrayList<Book>()
    lateinit var helper: NetworkHelper
    lateinit var mgr: DownloadManager
    var mPage = 1
    val mHandler = Handler()
    var future: Future<*>? = null
    var threadPoolExecutor = Executors.newSingleThreadExecutor()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissions()
        mgr = applicationContext.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

        helper = NetworkHelper(this.applicationContext)

        mRecyclerView = findViewById<RecyclerView>(R.id.recyclerView)

        mRecyclerView?.addOnItemTouchListener(RecyclerTouchListener(this, mRecyclerView!!,
                object : ClickListener {
                    override fun onClick(view: View, position: Int) {
                        val intent = Intent(view.context, BookDetail::class.java)

                        intent.putExtra("BookId", mBooks[position].id)
                        intent.putExtra("BookTitle", mBooks[position].title)
                        intent.putExtra("BookAuthor", mBooks[position].author)
                        intent.putExtra("BookDescription", mBooks[position].description)
                        startActivity(intent)
                    }

                    override fun onLongClick(view: View, position: Int) {
                        if (ContextCompat.checkSelfPermission(baseContext,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            FileSystem.downloadBook(applicationContext, mBooks[position])
                        }
                    }
                }))

        // Use grid layout manager
        mLayoutManager = GridLayoutManager(this, 2, GridLayout.VERTICAL, false)
        mRecyclerView?.layoutManager = mLayoutManager

        mAdapter = BookListAdapter(mBooks)
        mRecyclerView?.adapter = mAdapter

        // Fill view with books
        addBooks(mPage++)

        // Start download service
        // startService(Intent(this.applicationContext, DownloadService::class.java))
    }

    fun addBooks(page: Int) {
        future?.cancel(true)
        val mRunnableOnSeparateThread = Runnable {
            // do some long operation
            val html = helper.runSync("https://fantasy-worlds.org/lib/" + page)
            val books = ValueRecognition.parseLibrary(html)

            // who do we know that's associated with the UI thread?
            mHandler.post {
                mBooks.addAll(books)
                mRecyclerView?.adapter?.notifyDataSetChanged()
            }
        }
        future = threadPoolExecutor.submit(mRunnableOnSeparateThread)
    }

    fun searchBooks(q: String) {
        future?.cancel(true)
        mBooks.clear()
        mRecyclerView?.adapter?.notifyDataSetChanged()
        val mRunnableOnSeparateThread = Runnable {
            // do some long operation
            val html = helper.runSync("https://fantasy-worlds.org/search/?q=" + URLEncoder.encode(q, "UTF-8"))
            val books = ValueRecognition.parseLibrary(html)

            // who do we know that's associated with the UI thread?
            mHandler.post {

                if (books.isEmpty()) {

                }
                mBooks.addAll(books)
                mRecyclerView?.adapter?.notifyDataSetChanged()
            }
        }
        future = threadPoolExecutor.submit(mRunnableOnSeparateThread)
    }


    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)


        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu?.findItem(R.id.app_bar_search)?.actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setIconifiedByDefault(false)

        val queryTextListener = object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                if (newText == "") {
                    mBooks.clear()
                    mPage = 1
                    addBooks(mPage)
                } else {
                    searchBooks(newText)
                }
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // this is your adapter that will be filtered
                if (query == "") {
                    mBooks.clear()
                    mPage = 1
                    addBooks(mPage)
                } else {
                    searchBooks(query)
                }
                return false
            }
        }
        searchView.setOnQueryTextListener(queryTextListener)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.app_bar_search -> {
                Toast.makeText(this.applicationContext, "Search", Toast.LENGTH_SHORT).show()
            }
            R.id.app_bar_download -> {
                Toast.makeText(this.applicationContext, "Download", Toast.LENGTH_SHORT).show()
            }
            R.id.app_bar_settings -> {
                startActivity(Intent(baseContext, SettingsActivity::class.java))
            }
            else -> {
            }
        }
        return true
    }

    fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(baseContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this@MainActivity,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE)
        }
    }

    interface ClickListener {
        fun onClick(view: View, position: Int)
        fun onLongClick(view: View, position: Int)
    }

    // Recycler View downloads books on long click
    class RecyclerTouchListener(context: Context, recycleView: RecyclerView, private val clicklistener: ClickListener?) : RecyclerView.OnItemTouchListener {
        private val gestureDetector: GestureDetector

        init {
            gestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent): Boolean {
                    return true
                }

                override fun onLongPress(e: MotionEvent) {
                    val child = recycleView.findChildViewUnder(e.x, e.y)
                    if (child != null && clicklistener != null) {
                        clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child))
                    }
                }
            })
        }

        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            val child = rv.findChildViewUnder(e.x, e.y)
            if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
                clicklistener.onClick(child, rv.getChildAdapterPosition(child))
            }
            return false
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {

        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

        }
    }
}
