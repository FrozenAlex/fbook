package com.frozenalex.fbook.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.frozenalex.fbook.R
import com.frozenalex.fbook.data.model.Book
import com.frozenalex.fbook.util.FileSystem
import android.view.View.OnLongClickListener


class BookDetail : AppCompatActivity() {
    lateinit var book: Book


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_detail)

        // Intent madness
        val extra = intent.extras;
        val id = intent.extras.getLong("BookId")
        val title = intent.extras.getString("BookTitle")
        val author = intent.extras.getString("BookAuthor")
        val description = intent.extras.getString("BookDescription")

        book = Book(id, title, author, description);

        setTitle(title);
        findViewById<TextView>(R.id.bookDescription).setText(description);
        findViewById<TextView>(R.id.bookName).setText(title);
        findViewById<TextView>(R.id.bookAuthor).setText(author);

        Glide.with(baseContext)
                .load(book.thumbURL)
                .into((findViewById<ImageView>(R.id.bookCover)));

        findViewById<View>(R.id.bookCover).setOnClickListener {
            val intent = Intent(baseContext, FullscreenActivity::class.java)

            intent.putExtra("BookId", book.id)
            intent.putExtra("BookTitle", book.title)
            intent.putExtra("BookAuthor", book.author)
            intent.putExtra("BookDescription", book.description)

            startActivity(intent);
        }
        findViewById<View>(R.id.bookCover).setOnLongClickListener(OnLongClickListener {
            FileSystem.downloadCover(this.baseContext, book)
            true    // <- set to true
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.book_view_menu, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.app_bar_download -> {
                Toast.makeText(this.applicationContext, "Download", Toast.LENGTH_SHORT).show()
            }
            R.id.app_bar_settings -> {
                Toast.makeText(this.applicationContext, "Settings", Toast.LENGTH_SHORT).show()
                startActivity(Intent(baseContext, SettingsActivity::class.java))
            }
            else -> {
            }
        }
        return true;
    }

    fun downloadBook(view: View) {
        if (ContextCompat.checkSelfPermission(baseContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(baseContext, getString(R.string.message_no_permission),
                    Toast.LENGTH_SHORT).show();
        } else {
            FileSystem.downloadBook(applicationContext, book)
        }
    }
}
