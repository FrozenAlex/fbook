package com.frozenalex.fbook.download

import android.app.Application
import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.os.Message
import android.os.HandlerThread
import android.widget.Toast
import com.frozenalex.fbook.network.NetworkHelper
import okhttp3.Cache
import okhttp3.OkHttpClient


class DownloadService : Service() {

    val networkClient = OkHttpClient()
    //.cache(Cache(applicationContext.cacheDir)

    companion object {
        var instance: DownloadService? = null
        fun start() {

        }
    }

    @Volatile
    private lateinit var mHandlerThread: HandlerThread
    private lateinit var mServiceHandler: ServiceHandler

    override fun onCreate() {
        super.onCreate()
        mHandlerThread = HandlerThread("DownloadService.HandlerThread");
        mHandlerThread.start();
        // An Android service handler is a handler running on a specific background thread.
        mServiceHandler = ServiceHandler(mHandlerThread.getLooper());
        makeToast("Service created")
    }

    fun makeToast(string: String) = Toast.makeText(this.applicationContext, string, Toast.LENGTH_SHORT).show()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        // Send empty message to background thread
        mServiceHandler.sendEmptyMessageDelayed(0, 500)
        // or run code in background
        mServiceHandler.post {

            stopSelf()
        }
        return START_STICKY;
    }

    override fun onDestroy() {
        mHandlerThread.quit();
        super.onDestroy()
    }

    private inner class ServiceHandler(looper: Looper) : Handler(looper) {

        // Define how to handle any incoming messages here
        override fun handleMessage(message: Message) {
            makeToast("message: " + message.what)
            stopSelf();
        }
    }

    // Not used
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}