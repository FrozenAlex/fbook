package com.frozenalex.fbook.network

import android.content.Context
import android.widget.Toast
import okhttp3.OkHttpClient
import okhttp3.Request


class NetworkHelper(context: Context) {
    var context = context;
    var client = OkHttpClient()

    fun runSync(url: String): String {
        var request = Request.Builder()
                .url(url)
                .build()

        try {
            //Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
            return client.newCall(request).execute().body()!!.string()
        } catch (e: Exception) {
            //Toast.makeText(context,e.message,Toast.LENGTH_SHORT).show()
            return ""
        }

    }

    fun asyncGet(url: String) {

    }

    class AsyncMf() : Runnable {
        //client = client
        override fun run() {

        }
    }
}