package com.frozenalex.fbook

import com.frozenalex.fbook.data.model.Book
import com.frozenalex.fbook.util.ValueRecognition
import org.junit.Test
import org.junit.Assert.*
import io.reactivex.subscribers.TestSubscriber
import java.io.File


/**
 * Created by Alex Uskov on 7/24/17.
 */
public class BookUnitTest {

    @Test
    fun DateParserTest() {
        var book = Book(1, "title", "author", "description")
        assertEquals(4, (2 + 2).toLong())
    }

    @Test
    fun RatingParserTest() {
        assertEquals(2f, ValueRecognition.parseRating("Рейтинг: 2/10"))
        assertEquals(2.213f, ValueRecognition.parseRating("Рейтинг: 2.213/10"))
        assertEquals(2.313f, ValueRecognition.parseRating("Рейтинг: 2.313/10"))
        assertEquals(null, ValueRecognition.parseRating(""))
    }

    @Test
    fun LibraryParserTest() {
        // Get file URI
        var uri = this.javaClass.classLoader.getResource("LibPage.html")

        var file = File(uri.toURI())

        var text = file.readText();

        var books = ValueRecognition.parseLibrary(text)

        System.out.println(books.toString())

        assertEquals(4, (2 + 2).toLong())

//        val letters = Arrays.asList("A", "B", "C", "D", "E")
//        val subscriber = TestSubscriber<String>()
//
//        val observable = Observable
//                .from(letters)
//                .zipWith(
//                        Observable.range(1, Integer.MAX_VALUE),
//                        { string, index -> index + "-" + string })
//
//        observable.subscribe(subscriber)
//
//        subscriber.assertCompleted()
//        subscriber.assertNoErrors()
//        subscriber.assertValueCount(5)
//        assertThat(
//                subscriber.getOnNextEvents(),
//                hasItems("1-A", "2-B", "3-C", "4-D", "5-E"))
    }
}